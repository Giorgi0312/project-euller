/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicios_logica;

/**
 *
 * @author giorgi.silva
 */
public class Trocar_valor_duas_variaveis {

   
    public static void main(String[] args) {
        int a = 999;
        int b = 555;
        
        int temp = a;
        a = b;
        b = temp;
        System.out.println(a);
        System.out.println(b);
    }
    
}
