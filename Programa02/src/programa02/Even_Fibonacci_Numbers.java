/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programa02;
import java.io.*;
/**
 *
 * @author giorgi.silva
 */
public class Even_Fibonacci_Numbers {
    static int EvenFibSum(int limite)
    {
        if(limite < 2)
        {
            return 0;
        }
        
        long ef1 = 0, ef2 = 2;
        long sum = ef1 + ef2;
        
        while(ef2 <= limite)
        {
            long ef3  = 4 * ef2 + ef1;
            
            if(ef3 > limite)
            {
                break;
            }
            ef1 = ef2;
            ef2 = ef3;
            sum += ef2;
        }
        return(int) sum;
    }
    public static void main(String[] args) {
        int limite = 4000000;
        System.out.println(EvenFibSum(limite));
    }
    
}
