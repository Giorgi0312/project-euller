/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programa03;
import java.util.ArrayList;
/**
 *
 * @author giorgi.silva
 */
public class Largest_Prime_Factor {
    public static void main(String[] args) {
       
        long r  = maiorFatorPrimo(600851475143L);
        System.out.println(r);
    }
    
    private static long maiorFatorPrimo(long g)
    {
        long r = g;
        
        for(long i = 2; i < Math.sqrt(g); i++)
        {
            if(g % i == 0)
            {
                return maiorFatorPrimo(g/i);
            }
        }
        return r;
    }
}

